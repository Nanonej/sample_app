FROM ruby:alpine

RUN apk add --update \
      build-base \
      nodejs \
      postgresql-dev \
      tzdata
RUN gem install rails -v '5.1.4' --no-document

WORKDIR /app
COPY Gemfile /app/
COPY Gemfile.lock /app/

RUN bundle install --without production
COPY . .
CMD ["bundle","exec","puma","-C","config/puma.rb"]
